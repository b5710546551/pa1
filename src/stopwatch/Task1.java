package stopwatch;
/**
 * This task is to Append 100,000 Characters to A String.
 * It has can run the task and get the task name.
 * 
 * @author Chinatip Vichian
 */
public class Task1 implements Runnable{
	/* To run this task */
	public void run(){
		final char CHAR = 'a';

		String sum = ""; 
		int k = 0;
		while(k++ < 100000) {
			sum = sum + CHAR;
		}
	}
	
	/**
	 * Get String of details of the task
	 * @return String of what these task do
	 */
	public String toString(){
		return "Task 1: Append 100,000 Characters to A String";
	}
}
