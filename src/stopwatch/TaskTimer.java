package stopwatch;

/**
 * This TaskTimer class will call task from runnable.
 * It use StopWatch class to measure time each task take and print it out.
 * 
 * @author Chinatip Vichian
 */
public class TaskTimer {
	
	/** 
	 * It receives runnable from SpeedTest, work in it and measure how long its take.
	 * Then, it will print out name of a task and time of it.
	 * 
	 * @param runnable is tasks that want to know how long it will take to finish. 
	 */
	public void measureAndPrint(Runnable runnable){
		StopWatch timer = new StopWatch();
		System.out.println(runnable.toString());
		timer.start();
		runnable.run();
		timer.stop();
		System.out.printf("%.6f\n",timer.getElapsed());
	}
}
