package stopwatch;

import java.io.IOException;

/**
 * This SpeedTest class will run the tasks from Task1-5.
 * 
 * @author Chinatip Vichian
 */


/**
 * Task file for PA1-StopWatch.  Run this to try out your stopwatch,
 * then divide and rewrite it (Problem 2) to eliminate duplicate code.
 * 
 * This is a collection of common tasks involving strings and doubles,
 * to compare speed of using different data types.
 * 
 * This class has a lot of duplicate code.
 * Your job is to separate the code that is the same in each method
 * from the code that is different, and restructure it so that 
 * you can re-use the task-timer code that is the same.
 * 
 * When you get done you should have 7 classes:
 * A TaskTimer class (the reusable code) that can time any task
 * 5 task classes that implement Runnable
 * A Main class (application) to run the 5 tasks.
 * 
 * You can define the 5 task classes in the same source file as
 * the Main class or put them in separate files.
 * To define multiple classes in one source file, only one class
 * is declared "public" and the other classes omit the "public".
 */
public class SpeedTest {
	
	/**
	 * Run the tasks.
	 */
	public static void main(String[] args) throws IOException {
		TaskTimer taskTimer = new TaskTimer();
		taskTimer.measureAndPrint(new Task1());
		taskTimer.measureAndPrint(new Task2());
		taskTimer.measureAndPrint(new Task3());
		taskTimer.measureAndPrint(new Task4());
		taskTimer.measureAndPrint(new Task5());
	}
}