package stopwatch;
/**
 * This task is to Sum 100,000,000 Double Objects from An Array.
 * It has can run the task and get the task name.
 * 
 * @author Chinatip Vichian
 */
public class Task4 implements Runnable{
	/* Array of double objects*/
	private Double[] values = new Double[500000];
	/* To run this task */
	public void run(){
		for(int k=0; k<500000; k++) values[k] = new Double(k+1);
		Double sum = new Double(0.0);
		for(int count=0, i=0; count<100000000; count++, i++) {
			if (i >= 500000) i = 0;
			sum = sum + values[i];
		}

	}
	/*
	 * Get String of details of the task
	 * @return String of what these task do
	 */
	public String toString(){
		return "Task 4: Sum 100,000,000 Double Objects from An Array";
	}
}
