package stopwatch;

/**
 * A StopWatch that measures elapsed time between a starting time
 * and stopping time, or until the present time.
 * 
 * @author Chinatip Vichian
 */
public class StopWatch {
	/** time that the stopwatch was started, in nanoseconds. */
	private Long start =0L;
	/** time that the stopwatch was stopped, in nanoseconds. */
	private Long end =0L;
	
	/* Constructor for a stopwatch  */
	public StopWatch(){
	}
	
	/* Set and Run the stopwatch */
	public void start(){
		start = System.nanoTime();
		end = 0L;
	}
	/* Stop the stopwatch */
	public void stop(){
		end = System.nanoTime();
	}
	
	/**
	 * get elapsed time
	 * @return elapsed time in second
	 */
	public double getElapsed(){
		if(end==0)
			return (System.nanoTime()-start)/1.0E+9;
		return (end-start)/1.0E+9 ;
		
	}

	/**
	 * Check that the stopwatch is running or not
	 * @return 1) True, when the stopwatch is working.
	 * 2) False, when the stopwatch already stopped.
	 * 
	 */
	public boolean isRunning(){
		return end==0;
	}
}
