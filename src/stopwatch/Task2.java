package stopwatch;
/**
 * This task is to Append 100,000 Characters to A StringBuilder.
 * It has can run the task and get the task name.
 * 
 * @author Chinatip Vichian
 */
public class Task2 implements Runnable{
	/* To run this task */
	public void run(){
		final char CHAR = 'a';

		StringBuilder builder = new StringBuilder(); 
		int k = 0;
		while(k++ < 100000) {
			builder = builder.append(CHAR);
		}
	}
	/*
	 * Get String of details of the task
	 * @return String of what these task do
	 */
	public String toString(){
		return "Task 2: Append 100,000 Characters to A StringBuilder";
	}
}
