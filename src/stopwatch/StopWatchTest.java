package stopwatch;
/**
 * This class is for testing StopWatch class
 * 
 */
public class StopWatchTest {
	public static void doSomething(){
		int a =0;
		for(int i=0;i<11;i++){
			a+=i;
		}

	}
	
	public static void main(String [] args){
		StopWatch timer = new StopWatch( );
		System.out.println("Starting task");
		timer.start( );
		doSomething( ); // do some work
		System.out.printf("elapsed = %.6f sec\n", timer.getElapsed() );
		if ( timer.isRunning() ) 
			System.out.println("timer is running");
		else System.out.println("timer is stopped");
		doSomething( ); // do more work
		timer.stop( ); // stop timing the work
		System.out.printf("elapsed = %.6f sec\n", timer.getElapsed() );
		if ( timer.isRunning() ) System.out.println("timer is running");
		else System.out.println("timer is stopped");
		System.out.printf("elapsed = %.3f sec\n", timer.getElapsed() );

	}
}
