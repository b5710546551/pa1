package stopwatch;

import java.math.BigDecimal;
/**
 * This task is to Sum 100,000,000 BigDecimal Objects from An Array.
 * It has can run the task and get the task name.
 * 
 * @author Chinatip Vichian
 */
public class Task5 implements Runnable{
	/* Array of BigDecimal objects*/
	private BigDecimal[] values = new BigDecimal[500000];
	/* To run this task */
	public void run(){
		for(int k=0; k<500000; k++) values[k] = new BigDecimal(k+1);
		BigDecimal sum = new BigDecimal(0.0);
		for(int count=0, i=0; count<100000000; count++, i++) {
			if (i >= 500000) i = 0;
			sum = sum.add( values[i] );
		}
		
	}
	/*
	 * Get String of details of the task
	 * @return String of what these task do
	 */
	public String toString(){
		return "Task 5: Sum 100,000,000 BigDecimal Objects from An Array";
	}
}
